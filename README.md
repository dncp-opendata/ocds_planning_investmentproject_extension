# Planning: Investment Project

In Paraguay as in other countries, there are, as infraestructure project like 
CoST, another kind of projects, investment project, which can be linked to its 
corresponding contracts in the planning and contract level of the contracting process.
To make this link, this extension adds a `investmentProjects` array inside the
`planning` and `contracts` block with the id, name and uri of the Investment Project to which a 
planning belongs to track the link between the project and its contracts.

## Example

```json
{
  "planning": {
      "investmentProjects": [{
        "id": "566",
        "name": "Ampliación y duplicación de las rutas 2 y 7",
        "uri": "http://mapainversionessnip.economia.gov.py/proyecto/perfilproyecto/566"
      },
      {
        "id": "567",
        "name": "Other project",
        "uri": "http://mapainversionessnip.economia.gov.py/proyecto/perfilproyecto/567"
      }
    ]
  },
  "contracts": [
    {
        "id": "1",
        "investmentProjects": [{
        "id": "566",
        "name": "Ampliación y duplicación de las rutas 2 y 7",
        "uri": "http://mapainversionessnip.economia.gov.py/proyecto/perfilproyecto/566"
      }]
    },
    {
        "id": "2",
        "investmentProjects": [{
        "id": "567",
        "name": "Other project",
        "uri": "http://mapainversionessnip.economia.gov.py/proyecto/perfilproyecto/5676"
      }]
    }
  
  ]
}
```

